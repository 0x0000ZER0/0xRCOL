SRC=main.c
OUT=rcol

debug:
	gcc -g $(SRC) -DZ0_DEBUG -o $(OUT) -lm \
		-fsanitize=leak,undefined,address -Wall -Wextra

release:
	gcc -Os $(SRC) -o $(OUT) -lm
