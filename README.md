# 0xRCOL

A resistor color code calculator.

### Usage

There are 2 commands which can be used. In case of not remembering the commands you can type the executable name and it'll yield the usage:

```
> rcol

=========== resistor color code calculator ===========

- codes {NUMBER_OF_BANDS}                                        	 will print a table with all color codes.
- calc  {BAND_1_COLOR_ID} {BAND_2_COLOR_ID} .. {BAND_N_COLOR_ID} 	 calculates the resistance with given color ids.
```

- To get the color ids and extra information about them simply type `rcol codes` and in the new argument the number of bands you got on your resistor:

```
> rcol codes 4

=================== color table ==================
| color (id) | band 1 | band 2 | band 3 | band 4 |
==================================================
| black  (0) |   --   |    0   |  1     |   --   |
==================================================
| brown  (1) |    1   |    1   |  10    |  ±1%   |
==================================================
| red    (2) |    2   |    2   |  100   |  ±2%   |
==================================================
| orange (3) |    3   |    3   |  10^3  | ±0.05% |
==================================================
| yellow (4) |    4   |    4   |  10^4  | ±0.02% |
==================================================
| green  (5) |    5   |    5   |  10^5  | ±0.5%  |
==================================================
| blue   (6) |    6   |    5   |  10^6  | ±0.25% |
==================================================
| violet (7) |    7   |    7   |  10^7  | ±0.1%  |
==================================================
| grey   (8) |    8   |    8   |  10^8  | ±0.01% |
==================================================
| white  (9) |    9   |    9   |  10^9  |   --   |
==================================================
| gold   (10)|   --   |   --   |  10^-1 |  ±5%   |
==================================================
| silver (11)|   --   |   --   |  10^-2 |  ±10%  |
==================================================
| pink   (12)|   --   |   --   |  10^-3 |   --   |
==================================================
```

- To calculate the resistance type the color ids in order each of them in a new argument:

```
> rcol calc 1 2 0 3

Resistance (DEFAULT): 12.00 Ω
Resistance (MINIMUM): 11.99 Ω
Resistance (MAXIMUM): 12.01 Ω
```

### Build

Building the project is extremly simple:

1. clone the project.
2. run `make release`. 
